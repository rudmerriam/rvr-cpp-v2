//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: rvr_tcp.cpp
//
//     Author: rmerriam
//
//    Created: Nov 12, 2021
//
//======================================================================================================================

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
/*
 * check "send" vs "write"
 * select for testing if data ready for read
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include <Trace.h>
#include "TcpClient.h"
#include "SerialPort.h"
//----------------------------------------------------------------------------------------------------------------------
inline unsigned char const* uc_str(char const* s) {
    return reinterpret_cast<unsigned char const*>(s);
}
//----------------------------------------------------------------------------------------------------------------------
void network_test() {
    using namespace std::chrono_literals;

    //    std::string const addr = "192.168.1.235";
    std::string addr = "ESPSerial";

    std::string const port = "7373";
    rvr::TcpClient tcp { addr, port };
    mys::tout << code_line;

    if ( !tcp) {
        std::cout << "tcp not open";
    }
    rvr::ustring msg { uc_str("hello world\n") };
    tcp.write(msg);
    std::this_thread::sleep_for(2000ms);

    for (int i {}; i < 1000; ++i) {
        tcp.write(msg);
        std::this_thread::sleep_for(500ms);
    }

}
//----------------------------------------------------------------------------------------------------------------------
void serial_test() {
    std::string const port { "/dev/rvr" };
    rvr::SerialPort serial { port, 115200 };

}
//----------------------------------------------------------------------------------------------------------------------
int main() {
    network_test();
//    serial_test();
    exit(EXIT_SUCCESS);
}

