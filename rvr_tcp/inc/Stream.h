#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: Stream.h
//
//     Author: rmerriam
//
//    Created: Jun 27, 2021
//
//======================================================================================================================

#include <unistd.h>

#include <iostream>

namespace rvr {
    using ustring = std::basic_string<uint8_t>;

    class Stream {
    public:
        Stream() {
        }
        virtual ~Stream() {
            ::close(mCommId);
        }

        Stream(Stream const& other) = delete;
        Stream(Stream&& other) = delete;
        Stream& operator=(Stream const& other) = delete;
        Stream& operator=(Stream&& other) = delete;
        //----------------------------------------------------------------------------------------------------------------------
        operator bool() const {
            return mIsOpen;
        }
        //----------------------------------------------------------------------------------------------------------------------
        int64_t read(uint8_t buffer[], uint32_t const len) const {
            return ::read(mCommId, buffer, len);
        }
        //----------------------------------------------------------------------------------------------------------------------
        auto read() -> uint8_t const {
            uint8_t ch {};

            if (::read(mCommId, &ch, 1) < 0) {
                return -1;
            }
            return ch;
        }
        //----------------------------------------------------------------------------------------------------------------------
        ssize_t write(ustring const& msg) const {
            return ::write(mCommId, msg.c_str(), msg.size());
        }
        //----------------------------------------------------------------------------------------------------------------------
        auto write(uint8_t const& ch) -> int64_t const {
            return ::write(mCommId, &ch, 1);
        }

    protected:
        // set by deroved class
        int mCommId { -1 };
        bool mIsOpen { true };

    };

} /* namespace rvr */
