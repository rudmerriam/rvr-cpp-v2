//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: drive.cpp
//
//     Author: rmerriam
//
//    Created: Oct 29, 2021
//
//======================================================================================================================
#include <thread>

#include "rvr++.h"

//using rvr::CommandResponse;
//using enum struct rvr::CommandResponse;
rvr::CommandResponse resp_yes = rvr::CommandResponse::resp_yes;
//---------------------------------------------------------------------------------------------------------------------
void cmds(rvr::Drive& drive) {
    mys::tout << code_line;

//    mys::tout << code_line << "RV drive";
//    drive.driveRcNormalized(5, 50, 1, resp_yes);
//    drive.driveRcSI(30.0, 0.0, 1, resp_yes);

//    mys::tout << code_line << "tank drive";
//    drive.driveTankNormalized(5, -5, resp_yes);
//    drive.driveTankSI(.5, -.5, resp_yes);

    mys::tout << code_line << "yaw drive";
    drive.driveYawNormalized(30, 30, resp_yes);
    drive.driveYawSI(30.0, 0.25, resp_yes);

//    mys::tout << code_line << "drive to position";
//    driveToPositionNormalized
//    driveToPositionSI

//    drive
//    driveWithHeading
    std::this_thread::sleep_for(2000ms);

    mys::tout << code_line;
}
void check_control_system(rvr::Drive& drive) {
    drive.getActiveControlSystemId();
    std::this_thread::sleep_for(50ms);
    mys::tout << code_line << (uint8_t)drive.activeControlSystemId().get_or() << mys::tab << drive.activeControlSystemIdText().get_or();
}
//---------------------------------------------------------------------------------------------------------------------
void control(rvr::Drive& drive) {
    mys::tout << code_line;

    drive.getStopControllerState(resp_yes);
    std::this_thread::sleep_for(50ms);
    mys::tout << code_line << "isStopControllerStopped" << mys::tab << drive.isStopControllerStopped().get_or();

    drive.restoreDefaultControlSystemTimeout(resp_yes);

    drive.driveYawNormalized(0, 0, resp_yes);
//    drive.driveYawSI(30.0, 0.25, resp_yes);

    drive.getStopControllerState(resp_yes);
    std::this_thread::sleep_for(5000ms);
    mys::tout << code_line << "isStopControllerStopped" << mys::tab << drive.isStopControllerStopped().get_or();

    mys::tout << code_line << "getActiveControlSystemId";

    drive.restoreInitialDefaultControlSystems(resp_yes);
    check_control_system(drive);

    drive.setActiveControlSystemId(rvr::Drive::control_system_type_drive_with_yaw, rvr::Drive::ControlSystemId::drive_with_yaw_advanced_mode,
                                   resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);

    check_control_system(drive);
    drive.stop();

    drive.setActiveControlSystemId(rvr::Drive::control_system_type_drive_with_yaw, rvr::Drive::ControlSystemId::drive_with_yaw_basic_mode, resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);

    check_control_system(drive);

    drive.setCustomControlSystemTimeout(500, resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);
    std::this_thread::sleep_for(500ms);

    mys::tout << code_line;
}
//---------------------------------------------------------------------------------------------------------------------
void slew(rvr::Drive& drive) {
    mys::tout << code_line;

    // get default params
    drive.restoreDefaultDriveTargetSlewParameters(resp_yes);
    std::this_thread::sleep_for(50ms);
    drive.getDriveTargetSlewParameters(resp_yes);
    std::this_thread::sleep_for(50ms);

    {
        auto [a, b, c, l, f] = drive.targetSlewParameters().get_or();
        mys::tout << code_line << "targetSlewParameters" << mys::sp << a << mys::sp << b << mys::sp << c << mys::sp << l << mys::sp << f;
    }

    // set new params
    rvr::Drive::SlewRateParams slew_params = { -30, 400, 100, 4, rvr::Drive::LinearVelocitySlewMethods::constant };
    drive.setDriveTargetSlewParameters(slew_params, resp_yes);
    std::this_thread::sleep_for(50ms);

    drive.getDriveTargetSlewParameters(resp_yes);
    std::this_thread::sleep_for(50ms);
    {
        auto [a, b, c, l, f] = drive.targetSlewParameters().get_or();
        mys::tout << code_line << "targetSlewParameters" << mys::sp << a << mys::sp << b << mys::sp << c << mys::sp << l << mys::sp << f;
    }
}
//---------------------------------------------------------------------------------------------------------------------
void location(rvr::Drive& drive, rvr::SensorsDirect& sen_d) {
    mys::tout << code_line;

    sen_d.resetLocatorXY(resp_yes);
    std::this_thread::sleep_for(50ms);

    drive.driveToPositionSI(0.0, 0.0, 1.0, 1.0, rvr::Drive::ModeFlags::forward, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }

    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();
//
    drive.driveToPositionSI(0.0, 0.0, 0.0, 1.0, rvr::Drive::ModeFlags::reverse, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }
    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();

    drive.driveToPositionSI(0.0, 0.0, 1.0, 1.0, rvr::Drive::ModeFlags::forward, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }

    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();
//
    drive.driveToPositionSI(0.0, 0.0, 0.0, 1.0, rvr::Drive::ModeFlags::reverse, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }
    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();

    std::this_thread::sleep_for(250ms);

    //----------------------------------------
    drive.driveToPositionNormalized(0, 0.0, 1.0, 64, rvr::Drive::ModeFlags::forward, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }
    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();

    drive.driveToPositionNormalized(0, 0.0, 0.0, 64, rvr::Drive::ModeFlags::forward, resp_yes);
    while ( !drive.isDriveToPositionDone().get_or()) {
        std::this_thread::sleep_for(25ms);
    }
    mys::tout << code_line << "isDriveToPositionDone? " << drive.isDriveToPositionDone().get_or();

    mys::tout << code_line;
}

void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d) {
    mys::tout << code_line;
//    cmds(drive);
//    control(drive);
    location(drive, sen_d);
//    slew(drive);

    mys::tout << code_line;
}
