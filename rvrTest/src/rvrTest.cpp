//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>
using namespace std::literals;

#include <Trace.h>
#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void direct(rvr::SensorsDirect& sen_d);
void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d);
void notifications(rvr::SensorsDirect& sen_d);
void leds_test(rvr::IoLed& led);
void power(rvr::Power& pow);
void streaming(rvr::SensorsStream& sen_s);
void sysinfo(rvr::SystemInfo& sys, rvr::Connection& cmd, rvr::ApiShell& api);

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    mys::TraceOff tdbg_on { mys::terr };

    auto stream = rvr::Stream::make_stream(argc, argv);

    rvr::SendPacket req { *stream };
    rvr::ReadPacket in_packet { *stream };
    rvr::Blackboard bb;
//---------------------------------------------------------------------------------------------------------------------
//  Setup the response thread to read responses
    std::promise<void> end_tasks;
    std::shared_future<void> end_future(end_tasks.get_future());
    rvr::Response resp { in_packet, bb, end_future };
    mys::tout << code_line << "----------------";

    auto resp_future = std::async(std::launch::async, std::ref(resp));
//---------------------------------------------------------------------------------------------------------------------
    rvr::ApiShell api(bb, req);
    rvr::Connection cmd(bb, req);
    rvr::Drive drive(bb, req);
    rvr::IoLed led(bb, req);
    rvr::Power pow(bb, req);
    rvr::SensorsDirect sen_d(bb, req);
    rvr::SensorsStream sen_s(bb, req);
    rvr::SystemInfo sys(bb, req);

    pow.awake(rvr::CommandResponse::resp_yes);

    mys::tout << code_line << "is awake:  " << pow.isWakeNotify().get_or();
    std::this_thread::sleep_for(200ms);
    mys::tout << code_line << "is awake:  " << pow.isWakeNotify().get_or();
//=====================================================================================================================
    try {

        direct(sen_d);
//        driving(drive, sen_d);
        leds_test(led);
        notifications(sen_d);
        power(pow);
        streaming(sen_s);
        sysinfo(sys, cmd, api);
    }
    catch (std::exception& e) {
        mys::terr << code_line << e.what() << "=================================";
    }

    mys::tout << code_line << "sleep" << mys::nl;
    pow.sleep(rvr::CommandResponse::resp_yes);

    std::this_thread::sleep_for(1s);
    end_tasks.set_value();
    resp_future.get();

    bb.dump();

    return 0;
}

