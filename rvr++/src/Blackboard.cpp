//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:                                  //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <vector>

#include <Trace.h>

#include <Blackboard.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {

    //----------------------------------------------------------------------------------------------------------------------
    rvr::Blackboard::key_t Blackboard::msgKey(Devices const dev, uint8_t const cmd, uint8_t const seq) {

        Blackboard::key_t key;

        if (seq >= 0x80) {
            key = Blackboard::entryKey(dev, cmd);
        }
        else {
            key = Blackboard::entryKey(dev, cmd, seq);
        }
        return key;
    }
    //----------------------------------------------------------------------------------------------------------------------
    std::string Blackboard::entryName(key_t key) {
        std::string s;
        mys::TraceOff tdbg_ctrl { mys::tout };
        mys::tout << code_line << "key: " << std::hex << key << mys::sp << key_s(key) << std::dec;

        auto it { mDictionary.find(key) };

        if (it == mDictionary.end()) {
            it = mDictionary.find(key & 0xFFFFFF00);
            mys::tout << code_line << "key: " << std::hex << (key & 0xFFFFFF00);
        }
        if (it != mDictionary.end()) {
            s = it->second.name;
            mys::tout << code_line << "key: " << std::hex << key << mys::tab << s;
        }
        return s;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::addEntryValue(key_t const key, RvrMsg value) {
        mDictionary[key].value = value;
    }
    //----------------------------------------------------------------------------------------------------------------------
    RvrMsgView Blackboard::entryValue(Devices const dev, uint8_t const cmd, uint8_t const id) const {
        RvrMsgView msg_opt { entryValue(entryKey(dev, cmd, id)) };
        return msg_opt;
    }
    //----------------------------------------------------------------------------------------------------------------------
    RvrMsgView Blackboard::entryValue(key_t const key) const {
        RvrMsgView res;
        if (auto it = mDictionary.find(key); (it != mDictionary.end()) & ( !it->second.value.empty())) {
            res = RvrMsgView { it->second.value };
        }
        return res;
    }
    //======================================================================================================================
    //  Method to put RvrMsg data into dictionary
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::msgArray(Blackboard::key_t key, uint8_t const cmd, uint8_t const src, RvrMsg::iterator begin, RvrMsg::iterator end) {
        mys::TraceOff tdbg_ctrl { mys::tout };

        RvrMsg msg { begin, end };
        mys::tout << code_line << mys::tab << std::hex << key << mys::sp << msg << std::dec;

        if (msg.empty()) {
            msg.push_back(0xFF);
        }
        else if (msg.size() >= 2) {
            uint8_t seq { msg.front() };

            if (seq == 0xFF) {    // either a stream or notification
                if (cmd == 0x3D) {    // stream
                    msg.erase(0, 2);
                }
                else {
                    msg.erase(0, 1);    // remove 0xFF and stream byte
                }
            }
            else if ((seq < 0x80) && (seq >= 0x04)) {
                // message seq has special flags that are not sequence number (> 0x80) or ids (< enable (0x20) - its a hack
                // because of the protocol
                switch (seq) {
                    // special case for temperatures
                    case (int)TemperatureIndexes::left_motor_temperature:
                    case (int)TemperatureIndexes::right_motor_temperature:
                    case (int)TemperatureIndexes::nordic_die_temperature:
                        msg.erase(0, 3);
                        break;

                        // the response from enable / disable messages don't report wthe type of the request
                        // so special sequence numbers are used to indicate the type of request
                    case SpecialSeq::enable:
                        key &= static_cast<Blackboard::key_t>(0xFFFFFF00);
                        msg[0] = true;
                        break;

                    case SpecialSeq::disable:
                        key &= static_cast<Blackboard::key_t>(0xFFFFFF00);
                        msg[0] = false;
                        break;
                }
            }
            else if (msg.size() > 2) {    // response with data
                mys::tout << code_line << mys::tab << std::hex << key << mys::tab << msg << std::dec;
                msg.erase(0, 2);
            }
            else {    // response with no data - flag that is handled
                mys::tout << code_line << mys::tab << std::hex << key << mys::tab << msg << std::dec;
                msg.erase(0, 1);
            }

        }
        mys::tout << code_line << mys::tab << std::hex << key << mys::tab << msg << std::dec;
        addEntryValue(key, msg);
    }
    //======================================================================================================================
    void Blackboard::clearEntry(Devices const dev, uint8_t const cmd) {
//        clearEntryValue(entryKey(dev, cmd, 0));
        mDictionary[entryKey(dev, cmd, 0)].value.clear();

    }
    //======================================================================================================================
    std::ostream& operator <<(std::ostream& os, Blackboard::key_s const& key) {
        os << std::hex << std::uppercase << std::right;
        os << std::setfill('0') << std::setw(2) << (uint16_t)key.key.bits_v.dev << mys::sp    //
           << std::setw(2) << (uint16_t)key.key.bits_v.cmd << mys::sp    //
           << std::setw(2) << (uint16_t)key.key.bits_v.id;
        return os;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::dump() {
        struct v_map {
            Blackboard::key_t key;
            Blackboard::BlackboardEntry be;
        };

        std::vector<v_map> vec;
        for (auto b : rvr::Blackboard::mDictionary) {
            v_map v { b.first, b.second };
            vec.push_back(v);
        }

        std::sort(vec.begin(), vec.end(),                       //
                  [](v_map const& a, v_map const& b) {
                      return a.key < b.key;
                  }
        );

        for (auto& i : vec) {
            mys::tout << (key_s)i.key << mys::sp << std::setw(45) << std::setfill(' ') << std::left << i.be.name << mys::tab << i.be.value;
        }
    }
}
/* namespace rvr */
