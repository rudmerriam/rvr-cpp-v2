//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:           //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>
using namespace std::literals;

#include <Trace.h>
#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void direct(rvr::SensorsDirect& sen_d);
void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d);
void notifications(rvr::SensorsDirect& sen_d);
void power(rvr::Power& pow);
void streaming(rvr::SensorsStream& sen_s, rvr::SensorsDirect& sen_d);
void sysinfo(rvr::SystemInfo& sys, rvr::Connection& cmd, rvr::ApiShell& api);
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    mys::TraceOff terr_ctrl { mys::terr };
    mys::TraceOn tout_ctrl { mys::tout };

    auto stream = rvr::Stream::make_stream(argc, argv);

    rvr::SendPacket requests { *stream };
    rvr::Blackboard bb;
    //---------------------------------------------------------------------------------------------------------------------
    //  Setup the response thread to read responses
    rvr::ReadPacket in_packet { *stream };

    std::promise<void> end_tasks;
    std::shared_future end_future(end_tasks.get_future());
    rvr::Response resp { in_packet, bb, end_future };

    auto resp_future = std::async(std::launch::async, std::ref(resp));
    //---------------------------------------------------------------------------------------------------------------------
    rvr::ApiShell api(bb, requests);
    rvr::Connection cmd(bb, requests);
    rvr::Drive drive(bb, requests);
    rvr::Power pow(bb, requests);
    rvr::SensorsDirect sen_d(bb, requests);
    rvr::SensorsStream sen_s(bb, requests);
    rvr::SystemInfo sys(bb, requests);

    sen_d.disableEverything(rvr::CommandResponse::resp_yes);
    std::this_thread::sleep_for(250ms);

    pow.awaken();

    mys::tout << code_line << "is awake:  " << pow.isAwake();
    while ( !pow.isAwake()) {
        std::this_thread::sleep_for(50ms);
    }
    mys::tout << code_line << "is awake:  " << pow.isAwake();
    //=====================================================================================================================
    try {
        mys::TraceOn tout_ctrl { mys::tout };

        sen_d.disableEverything(rvr::CommandResponse::resp_yes);
//        direct(sen_d);
//        driving(drive, sen_d);
//        notifications(sen_d);
//        streaming(sen_s, sen_d);
        power(pow);
//        sysinfo(sys, cmd, api);

        mys::tout << code_line << "done" << mys::nl;
    }
    catch (std::exception& e) {
        mys::terr << code_line << e.what() << "=================================";
    }

    mys::tout << code_line << "sleep" << mys::nl;
    pow.sleep(rvr::CommandResponse::resp_yes);

    std::this_thread::sleep_for(200ms);
    end_tasks.set_value();
    resp_future.get();

    bb.dump();

    return 0;
}

